﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Networking;
using UnityEngine;
public class CustomNetworkManager : NetworkManager
{
	[SerializeField]
	List<Transform> spawnerPoints;

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		var player = Instantiate(playerPrefab, new Vector3(0, 1000, 0), Quaternion.identity) as GameObject;
		player.transform.position = new Vector3(0, 100, 0);
		Debug.Log("create player instance");
		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
 	}
	public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);
		Debug.Log("PlayerCOnencted");
  	}
	
	public void OnSpawnPlayer(NetworkConnection conn){

		GameObject go = Instantiate(playerPrefab, new Vector3(0, 100, 0), Quaternion.identity); //локально на сервере создаем gameObject
		
		NetworkServer.Spawn(go);
	}
	 
 }

